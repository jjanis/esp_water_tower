#include "main.h"

//Init function 
void ICACHE_FLASH_ATTR user_init()
{
	// Initialize the GPIO subsystem.
	gpio_init();

	// Obligatory since SDK 1.1.0
	user_rf_pre_init();

	uart_div_modify(0, UART_CLK_FREQ / 115200);

	//Start os task
	system_os_task(user_procTask, user_procTaskPrio,user_procTaskQueue, user_procTaskQueueLen);

	system_init_done_cb(InitDoneCB);
}

//Do nothing function
static void ICACHE_FLASH_ATTR user_procTask(os_event_t *events)
{
    os_delay_us(10);
}

void ICACHE_FLASH_ATTR InitDoneCB(void)
{
	initPumpControl();

	connectToWiFi();
}