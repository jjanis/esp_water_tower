#ifndef WIFI_H
#define WIFI_H

#include "user_interface.h"
#include "ets_sys.h"
#include "osapi.h"
#include "os_type.h"
#include "espconn.h"
#include "mem.h"
#include "pumpControl.h"
#include "webServer.h"

#define SSID "edi-wlan"
#define PWD "123qwe123qwe"
#define PORT 3333

#define BUF_SIZE 32

void eventHandler(System_Event_t *event);
void ICACHE_FLASH_ATTR connectToWiFi();
void ICACHE_FLASH_ATTR createUdpSocket();
void ICACHE_FLASH_ATTR createTcpSocket();
void ICACHE_FLASH_ATTR connectCB(void *arg);
void ICACHE_FLASH_ATTR sentCB(void *arg);
void ICACHE_FLASH_ATTR disconnectCB(void *arg);
void ICACHE_FLASH_ATTR recvCB(void *arg, char *pData, unsigned short len);

#endif // !WIFI_H