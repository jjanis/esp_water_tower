#include "pumpControl.h"

void ICACHE_FLASH_ATTR initPumpControl()
{
	//Set GPIO12 to GPIO
	PIN_FUNC_SELECT(PERIPHS_IO_MUX_MTDI_U, FUNC_GPIO12);

	//Set GPIO12 high
	GPIO_OUTPUT_SET(PUMP_START_PIN, 1);

	//Set GPIO13 to GPIO
	PIN_FUNC_SELECT(PERIPHS_IO_MUX_MTCK_U, FUNC_GPIO13);

	//Set GPIO13 high
	GPIO_OUTPUT_SET(PUMP_STOP_PIN, 1);

	//Set GPIO14 to GPIO
	PIN_FUNC_SELECT(PERIPHS_IO_MUX_MTMS_U, FUNC_GPIO14);

	// Set GPIO14 to input
	GPIO_DIS_OUTPUT(PUMP_STATE_PIN);

	// Setup wifi status pin, low when got IP
	wifi_status_led_uninstall();
	wifi_status_led_install(LED_PIN, PERIPHS_IO_MUX_GPIO5_U, FUNC_GPIO5);

	// Init timers
	os_timer_setfn(&startPumpDone, startPumpDoneCb, NULL);
	os_timer_setfn(&stopPumpDone, stopPumpDoneCb, NULL);
	
	// try config from flash, revert to default if config not present
	if (!loadConfig(&pCS))
	{
		pCS.secElapsed = 0;
		pCS.shutdownAfter = 0;
		pCS.startingInterval = 60 * 60 * 12;
		pCS.shutdownAfter = 60 * 5;
		pCS.beginning = pCS.end = MAGIC_KEY;

		os_printf("Config on flash not found, reverting to default config: ");

		saveConfig(&pCS);
	}
	else
	{
		if (pCS.secElapsed < 0)
		{
			pCS.secElapsed = 0;
		}

		if (pCS.shutdownAfter < 0)
		{
			pCS.shutdownAfter = 0;
		}

		if (pCS.startingInterval < 0)
		{
			pCS.startingInterval = 60 * 60 * 12;
		}

		if (pCS.shutdownAfter < 0)
		{
			pCS.shutdownAfter = 60 * 5;
		}

		os_printf("Config on flash found: ");
	}

	os_printf("%d, %d, %d, %d\n", pCS.secElapsed, pCS.startingInterval, pCS.enabledTime, pCS.shutdownAfter);

	secSinceLastAction = 0;

	//Disarm timer
	os_timer_disarm(&secTimer);

	// setup timer (1000ms, repeating)
	os_timer_setfn(&secTimer, (os_timer_func_t *)secondTick, NULL);
	os_timer_arm(&secTimer, 1000, 1);
}

uint16_t ICACHE_FLASH_ATTR getState(uint8_t *buf)
{
	// sprintf is stored on iRAM, baaaaaad!
	return os_sprintf(buf, "%d,%d,%d,%d,%d", getPumpState(), pCS.secElapsed, pCS.startingInterval, pCS.enabledTime, pCS.shutdownAfter);
}

void ICACHE_FLASH_ATTR updateConfig(int si, int et)
{
	pCS.startingInterval = si;
	pCS.enabledTime = et;
}

void ICACHE_FLASH_ATTR secondTick(void *arg)
{
	if (pCS.shutdownAfter > 0)
	{
		if (--pCS.shutdownAfter == 0)
		{
			stopPumping();
		}
	}
	else
	{
		if (++pCS.secElapsed >= pCS.startingInterval)
		{
			startPumping();
		}
	}

	saveConfig(&pCS);

	secSinceLastAction++;
}

void ICACHE_FLASH_ATTR startPumping()
{
	if (secSinceLastAction < 10)
	{
		return;
	}

	GPIO_OUTPUT_SET(PUMP_START_PIN, 0);
	
	os_timer_arm(&startPumpDone, 1000, false);

	pCS.secElapsed = 0;
	pCS.shutdownAfter = pCS.enabledTime;
}

void ICACHE_FLASH_ATTR startPumpDoneCb(void *pArg)
{
	GPIO_OUTPUT_SET(PUMP_START_PIN, 1);
	secSinceLastAction = 0;
}

void ICACHE_FLASH_ATTR stopPumping()
{

	if (secSinceLastAction < 10)
	{
		return;
	}

	GPIO_OUTPUT_SET(PUMP_STOP_PIN, 0);

	os_timer_arm(&stopPumpDone, 1000, false);

	pCS.secElapsed = 0;
	pCS.shutdownAfter = 0;
}

void ICACHE_FLASH_ATTR stopPumpDoneCb(void *pArg)
{
	GPIO_OUTPUT_SET(PUMP_STOP_PIN, 1);
	secSinceLastAction = 0;
}

bool ICACHE_FLASH_ATTR getPumpState()
{
	return GPIO_INPUT_GET(PUMP_STATE_PIN);
}
