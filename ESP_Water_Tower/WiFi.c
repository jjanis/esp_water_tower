#include "WiFi.h"

uint8_t *buf;
char *si, *et;

bool sending;

void eventHandler(System_Event_t *event)
{
	switch (event->event)
	{
		case EVENT_STAMODE_CONNECTED:
			os_printf("Event: EVENT_STAMODE_CONNECTED\n");
			break;
		case EVENT_STAMODE_DISCONNECTED:
			os_printf("Event: EVENT_STAMODE_DISCONNECTED\n");
			break;
		case EVENT_STAMODE_AUTHMODE_CHANGE:
			os_printf("Event: EVENT_STAMODE_AUTHMODE_CHANGE\n");
			break;
		case EVENT_STAMODE_GOT_IP:
			os_printf("Event: EVENT_STAMODE_GOT_IP\n");
			createTcpSocket();
			createHttpServer();
			break;
		case EVENT_SOFTAPMODE_STACONNECTED:
			os_printf("Event: EVENT_SOFTAPMODE_STACONNECTED\n");
			break;
		case EVENT_SOFTAPMODE_STADISCONNECTED:
			os_printf("Event: EVENT_SOFTAPMODE_STADISCONNECTED\n");
			break;
		default:
			os_printf("Unexpected event: %d\n", event->event);
			break;
	}
}

void ICACHE_FLASH_ATTR connectToWiFi()
{
	struct station_config stationConfig;

	wifi_set_event_handler_cb(eventHandler);
	wifi_set_opmode_current(STATION_MODE);

	wifi_station_disconnect();

	os_strncpy(stationConfig.ssid, SSID, 32);
	os_strncpy(stationConfig.password, PWD, 64);

	wifi_station_set_config(&stationConfig);

	wifi_station_connect();

	wifi_station_set_auto_connect(1);
}

void ICACHE_FLASH_ATTR createUdpSocket()
{
	struct espconn *conn = (struct espconn *)os_zalloc(sizeof(struct espconn));

	conn->type = ESPCONN_UDP;
	conn->state = ESPCONN_NONE;
	conn->proto.udp = (esp_udp *)os_zalloc(sizeof(esp_udp));
	conn->proto.udp->local_port = PORT;

	// For UDP
	espconn_regist_recvcb(conn, recvCB);
	espconn_create(conn);

	os_printf("Created UDP socket on port %d\n", PORT);
}

void ICACHE_FLASH_ATTR createTcpSocket()
{
	struct espconn *conn = (struct espconn *)os_zalloc(sizeof(struct espconn));

	conn->type = ESPCONN_TCP;
	conn->state = ESPCONN_NONE;
	conn->proto.tcp = (esp_tcp *)os_zalloc(sizeof(esp_tcp));
	conn->proto.tcp->local_port = PORT;

	buf = (uint8_t *)os_zalloc(sizeof(uint8_t) * BUF_SIZE);
	sending = false;

	espconn_regist_sentcb(conn, sentCB);
	espconn_regist_connectcb(conn, connectCB);
	espconn_regist_disconcb(conn, disconnectCB);
	espconn_accept(conn);

	os_printf("Created TCP socket on port %d\n", PORT);
}

void ICACHE_FLASH_ATTR connectCB(void *arg)
{
	struct espconn *pNewEspConn = (struct espconn *)arg;

	os_printf("Client connected\n");

	espconn_regist_recvcb(pNewEspConn, recvCB);
}

void ICACHE_FLASH_ATTR sentCB(void *arg)
{
	//os_printf("Sending done\n");
	sending = false;
}

void ICACHE_FLASH_ATTR disconnectCB(void *arg)
{
	os_printf("Client disconnected\n");
}

void ICACHE_FLASH_ATTR recvCB(void *arg, char *pData, unsigned short len)
{
	uint16_t size;
	//os_printf("Received data!! - length = %d\n", len);

	struct espconn *pEspConn = (struct espconn *)arg;

	//os_printf("recvd = '%s'\n", pData);

	if (os_strstr(pData, STATE_CMD))
	{
		size = getState(buf);

		//os_printf("%d = '%s'\n", size, buf);

		if (sending)
		{
			os_printf("Sending too fast");
		}
		else
		{
			sending = true;
			espconn_send(pEspConn, buf, size);
		}
	}
	else if (os_strstr(pData, START_CMD))
	{
		startPumping();
	}
	else if (os_strstr(pData, STOP_CMD))
	{
		stopPumping();
	}
	else if (os_strstr(pData, CONFIG_CMD))
	{
		si = os_strchr(pData, ',');
		si++;

		et = os_strchr(si, ',');
		et++;
		os_printf("'%s' -> '%s'\n", si, et);
		updateConfig(atoi(si), atoi(et));
	}
}