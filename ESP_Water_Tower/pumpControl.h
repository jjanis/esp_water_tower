#ifndef PUMP_CONTROL_H
#define PUMP_CONTROL_H 1

#include "user_interface.h"
#include "osapi.h"

#include "storage.h"

#define PUMP_START_PIN 12
#define PUMP_STOP_PIN 13
#define PUMP_STATE_PIN 14

#define LED_PIN 5

#define STATE_CMD "State"
#define START_CMD "Start"
#define STOP_CMD "Stop"
#define CONFIG_CMD "Config"

struct PumpControlState pCS;

os_timer_t secTimer, startPumpDone, stopPumpDone;
uint16_t secSinceLastAction;

void ICACHE_FLASH_ATTR initPumpControl();
uint16_t ICACHE_FLASH_ATTR getState(uint8_t *buf);
void ICACHE_FLASH_ATTR updateConfig(int si, int et);
void ICACHE_FLASH_ATTR secondTick(void *arg);
void ICACHE_FLASH_ATTR startPumping();
void ICACHE_FLASH_ATTR startPumpDoneCb(void *pArg);
void ICACHE_FLASH_ATTR stopPumping();
void ICACHE_FLASH_ATTR stopPumpDoneCb(void *pArg);
bool ICACHE_FLASH_ATTR getPumpState();

#endif // !PUMP_CONTROL_H