#ifndef WEBSERVER_H
#define WEBSERVER_H

#include "user_interface.h"
#include "ets_sys.h"
#include "osapi.h"
#include "os_type.h"
#include "espconn.h"
#include "mem.h"
#include "pumpControl.h"

#define HTTP_PORT 80

#define BUF_SIZE 4096

#define WEB_START_CMD "GET /Start?"
#define WEB_STOP_CMD "GET /Stop?"

void ICACHE_FLASH_ATTR createHttpServer();
void ICACHE_FLASH_ATTR httpConnectCB(void *arg);
void ICACHE_FLASH_ATTR httpSentCB(void *arg);
void ICACHE_FLASH_ATTR httpDisconnectCB(void *arg);
void ICACHE_FLASH_ATTR httpRecvCB(void *arg, char *pData, unsigned short len);
uint16_t ICACHE_FLASH_ATTR getPumpStateHttp(void *buf);
#endif //WEBSERVER_H