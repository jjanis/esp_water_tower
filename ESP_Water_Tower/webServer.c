#include "webserver.h"

uint8_t *buf;
char *si, *et;

char *index_html =
"<!DOCTYPE html>\r\n"
"<html>"
"<head>"
"<meta http-equiv = 'refresh' content = '5; URL=/'>"
"</head>"
"<body>"
"<h1>Pump state</h1>"
"<table>"
"<tr><td>State:</td><td>%s</td></tr>"
"<tr><td>%s after:</td><td>%d s<br>"
"<tr><td>Starting interval:</td><td>%d s</td></tr>"
"<tr><td>Running interval:</td><td>%d s</td></tr>"
"<tr><td><form action = '/Start'>"
"<input type = 'submit' value = 'Start pump' />"
"</form></td><td>"
"<form action = '/Stop'>"
"<input type = 'submit' value = 'Stop pump' />"
"</form></td></tr></table>"
"</body>"
"</html>";

//"<link rel='shortcut icon' href='data:image/x-icon;', type='image/x-icon'> "
void ICACHE_FLASH_ATTR createHttpServer()
{
	struct espconn *conn = (struct espconn *)os_zalloc(sizeof(struct espconn));

	conn->type = ESPCONN_TCP;
	conn->state = ESPCONN_NONE;
	conn->proto.tcp = (esp_tcp *)os_zalloc(sizeof(esp_tcp));
	conn->proto.tcp->local_port = HTTP_PORT;

	espconn_regist_sentcb(conn, httpSentCB);
	espconn_regist_connectcb(conn, httpConnectCB);
	espconn_regist_disconcb(conn, httpDisconnectCB);
	espconn_accept(conn);

	os_printf("Created HTTP server on port %d\n", HTTP_PORT);
}


void ICACHE_FLASH_ATTR httpConnectCB(void *arg)
{
	struct espconn *pNewEspConn = (struct espconn *)arg;

	os_printf("Web client connected\n");

	espconn_regist_recvcb(pNewEspConn, httpRecvCB);
}

void ICACHE_FLASH_ATTR httpSentCB(void *arg)
{
	//os_printf("Sending done\n");
}

void ICACHE_FLASH_ATTR httpDisconnectCB(void *arg)
{
	os_printf("Web client disconnected\n");
}

void ICACHE_FLASH_ATTR httpRecvCB(void *arg, char *pData, unsigned short len)
{
	uint16_t size;

	uint8_t *html = (uint8_t *)os_zalloc(sizeof(uint8_t) * BUF_SIZE);
	buf = (uint8_t *)os_zalloc(sizeof(uint8_t) * BUF_SIZE);
	//os_printf("Received data!! - length = %d\n", len);

	//os_printf("recvd = '%s'\n", pData);

	struct espconn *pEspConn = (struct espconn *)arg;

	if (os_strstr(pData, WEB_START_CMD))
	{
		startPumping();
	}
	else if (os_strstr(pData, WEB_STOP_CMD))
	{
		stopPumping();
	}

	if (os_strstr(pData, "/favicon.ico"))
	{
		size = os_sprintf(buf, "HTTP/1.0 404 <CRLF>\r\n<CRLF>");
	}
	else
	{
		size = getPumpStateHttp(html);

		size = os_sprintf(buf, "HTTP/1.1 200 OK\r\n"
			"Content-Length: %d\r\n"
			"Content-Type: text/html\r\n"
			"Connection: Closed\r\n"
			"\r\n%s", os_strlen(html), html);
	}

	espconn_send(pEspConn, buf, size);
	
	os_free(html);
	os_free(buf);
}


uint16_t ICACHE_FLASH_ATTR getPumpStateHttp(void *buf)
{
	bool running = pCS.shutdownAfter > 0;//getPumpState() == 1;

	return os_sprintf(buf, index_html, running ? "Running" : "Stopped", running ? "Stop" : "Start", running ? pCS.shutdownAfter : pCS.startingInterval - pCS.secElapsed, pCS.startingInterval, pCS.enabledTime);
}