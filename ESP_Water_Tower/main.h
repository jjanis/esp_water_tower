#include "ets_sys.h"
#include "osapi.h"
#include "gpio.h"
#include "os_type.h"
#include "user_interface.h"
#include "WiFi.h"
#include "pumpControl.h"

#define user_procTaskPrio        0 
#define user_procTaskQueueLen    1

os_event_t    user_procTaskQueue[user_procTaskQueueLen];
static void user_procTask(os_event_t *events);

static void ICACHE_FLASH_ATTR user_procTask(os_event_t *events);
void eventHandler(System_Event_t *event);

void ICACHE_FLASH_ATTR InitDoneCB(void);