#include "storage.h"

bool ICACHE_FLASH_ATTR loadConfig(struct PumpControlState* pCS)
{
	if (spi_flash_read(USER_DATA_ADDR, (uint32_t *)pCS, sizeof(struct PumpControlState)) == SPI_FLASH_RESULT_OK)
	{
		return pCS->beginning ==  MAGIC_KEY && pCS->end == MAGIC_KEY;
	}

	return false;
}

bool ICACHE_FLASH_ATTR saveConfig(struct PumpControlState *pCS)
{
	// Must erase, else writting does not occur and no error is given
	spi_flash_erase_sector(USER_DATA_ADDR / SPI_FLASH_SEC_SIZE);

	return spi_flash_write(USER_DATA_ADDR, (uint32_t *)pCS, sizeof(struct PumpControlState)) == SPI_FLASH_RESULT_OK;
}