#include "os_type.h" 
#include "spi_flash.h"
#include "osapi.h"

/*
	4MB flash
	
	0x00000 = eagle.flash.bin, max len = 0x8000
	0x10000 = eagle.irom0text.bin, max len = 0x5C000

	0x10000 + 0x5C000 = 0x6C000

	max len values are stored in /SDK/ld/eagle.app.v6.ld:
	MEMORY
	{
	dport0_0_seg:                        org = 0x3FF00000, len = 0x10
										 dram0_0_seg : org = 0x3FFE8000, len = 0x14000
		iram1_0_seg : org = 0x40100000, len = 0x8000
		irom0_0_seg : org = 0x40210000, len = 0x5C000
	}
*/
#define USER_DATA_ADDR 0x6C000

#define MAGIC_KEY 0xB16B00B5

struct PumpControlState
{
	uint32_t beginning; // MAGIC_KEY
	uint32_t secElapsed;
	uint32_t shutdownAfter;
	uint32_t startingInterval;
	uint32_t enabledTime;
	uint32_t end; //MAGIC_KEY
};

bool ICACHE_FLASH_ATTR loadConfig(struct PumpControlState* pCS);
bool ICACHE_FLASH_ATTR saveConfig(struct PumpControlState* pCS);